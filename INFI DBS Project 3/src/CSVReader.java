import java.io.*;
import java.sql.SQLException;

public class CSVReader {

	public void read(String csv) throws ClassNotFoundException {
	String csvFile = "D:\\Dropbox\\Schule\\5aHWII\\Informationssysteme\\5ahwii-infi-dbp-project_3\\"+ csv +".csv";
    BufferedReader br = null;
    String line = "";
    String cvsSplitBy = ",";
    DBManager dbm = DBManager.getInstance();
    
    
    String[] csvFileNames = null;
    csvFileNames[1] = "AdU_nach_Branchen";
    csvFileNames[2] = "AdU_nach_Bundesland";
    csvFileNames[3] = "AdU_nach_Rechtsform";
    csvFileNames[4] = "AdU_nat_Person_Altersgruppen";
    csvFileNames[5] = "AdU_nat_Person_durchschnitt_Alter";
    csvFileNames[6] = "AdU_nat_Person_Frauenanteil";
    csvFileNames[7] = "AdU_nat_Person_geschl";
    
    
    try {

        br = new BufferedReader(new FileReader(csvFile));
        br.readLine();
        while ((line = br.readLine()) != null) {

            // use comma as separator
            String[] row = line.split(cvsSplitBy);
            for(int i = 0; i<= row.length; i++) {
            	if(row[i].contains("A10-")) {
            		row[i].replace("A10-", "");
            	}
            	else if(row[i].contains("B00")) {
            		if(row[i].contains("1")) row[i].replace("B00-1", "Burgenland");
            		else if(row[i].contains("2")) row[i].replace("B00-2", "Kärnten");
            		else if(row[i].contains("3")) row[i].replace("B00-3", "Niederösterreich");
            		else if(row[i].contains("4")) row[i].replace("B00-4", "Oberösterreich");
            		else if(row[i].contains("5")) row[i].replace("B00-5", "Salzburg");
            		else if(row[i].contains("6")) row[i].replace("B00-6", "Steiermark");
            		else if(row[i].contains("7")) row[i].replace("B00-7", "Tirol");
            		else if(row[i].contains("8")) row[i].replace("B00-8", "Vorarlberg");
            		else if(row[i].contains("9")) row[i].replace("B00-9", "Wien");
            		else {
            			row[i].replace("B00", "Österreich");
            		}
            	}
            	else if(row[i].contains("FOO2015")) {
            		if(row[i].contains("SP1")) row[i].replace("FOO2015-SP1", "Gewerbe und Handwerk");
            		else if(row[i].contains("SP2")) row[i].replace("FOO2015-SP2", "Industrie");
            		else if(row[i].contains("SP3")) row[i].replace("FOO2015-SP3", "Handel");
            		else if(row[i].contains("SP4")) row[i].replace("FOO2015-SP4", "Bank & Versicherung");
            		else if(row[i].contains("SP5")) row[i].replace("FOO2015-SP5", "Transport & Verkehr");
            		else if(row[i].contains("SP6")) row[i].replace("FOO2015-SP6", "Tourismus & Freizeit");
            		else if(row[i].contains("SP7")) row[i].replace("FOO2015-SP7", "Information & Consulting");
            		else {
            			row[i].replace("FOO2015", "Gesamtergebnis");
            		}
            	}
            	else if(row[i].contains("RF0")) {
            		if(row[i].contains("00")) row[i].replace("RF0-00", "Einzelunternehmen (nicht eingetragen)");
            		else if(row[i].contains("01")) row[i].replace("RF0-01", "Einzelunternehmen (eingetragen)");
            		else if(row[i].contains("02")) row[i].replace("RF0-02", "OG");
            		else if(row[i].contains("03")) row[i].replace("RF0-03", "KG");
            		else if(row[i].contains("04")) row[i].replace("RF0-04", "GmbH");
            		else if(row[i].contains("06")) row[i].replace("RF0-06", "AG");
            		else if(row[i].contains("09")) row[i].replace("RF0-09", "Verein");
            		else if(row[i].contains("66")) row[i].replace("RF0-66", "Sonstiges");
            		else {
            			row[i].replace("RF0", "Gesamtergebnis");
            		}
            	}
            	else if(row[i].contains("G11")) {
            		if(row[i].contains("-0")) row[i].replace("G11-0", "Unbekannt");
            		else if(row[i].contains("-1")) row[i].replace("G11-1", "Männer");
            		else if(row[i].contains("-2")) row[i].replace("G11-2", "Frauen");
            		else if(row[i].contains("-4")) row[i].replace("G11-4", "Frauenanteil");
            		else {
            			row[i].replace("G11", "Zusammen");
            		}
            	}
            	
            	else if(row[i].contains("AGE")) {
            		if(row[i].contains("00")) row[i].replace("AGE-00", "unbestimmt");
            		else if(row[i].contains("01")) row[i].replace("AGE-01", "unter 20");
            		else if(row[i].contains("02")) row[i].replace("AGE-02", "20 bis 30");
            		else if(row[i].contains("03")) row[i].replace("AGE-03", "30 bis 40");
            		else if(row[i].contains("04")) row[i].replace("AGE-04", "40 bis 50");
            		else if(row[i].contains("05")) row[i].replace("AGE-06", "50 bis 60");
            		else if(row[i].contains("06")) row[i].replace("AGE-09", "über 60");
            		else if(row[i].contains("88")) row[i].replace("AGE-66", "Durchschnittsalter");
            		
            	}
            }
            
            if(csvFile.equals(csvFileNames[1])) {
            	try {
					dbm.insertDataSectors(row[1], Integer.parseInt(row[2]), Integer.parseInt(row[3]));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	System.out.println("Written it into the Database");
            }
            else if(csvFile.equals(csvFileNames[2])) {
            	try {
					dbm.insertDataState(row[1], Integer.parseInt(row[2]), Integer.parseInt(row[3]));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	System.out.println("Written it into the Database");
            }
            else if(csvFile.equals(csvFileNames[3])) {
            	try {
					dbm.insertDataLegalForm(row[1],Integer.parseInt(row[2]), Integer.parseInt(row[3]));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	System.out.println("Written it into the Database");
            }
            else if(csvFile.equals(csvFileNames[4])) {
            	try {
					dbm.insertDataAge(row[1], Integer.parseInt(row[2]), Integer.parseInt(row[3]));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	System.out.println("Written it into the Database");
            }
            else if(csvFile.equals(csvFileNames[5])) {
            	try {
					dbm.insertDataAgeAverage(row[1], row[2], Integer.parseInt(row[3]), Integer.parseInt(row[4]));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	System.out.println("Written it into the Database");
            }
            else if(csvFile.equals(csvFileNames[6])) {
            	try {
					dbm.insertDataWomenPercentage(row[1], row[2], Integer.parseInt(row[3]), Integer.parseInt(row[4]));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	System.out.println("Written it into the Database");
            }
            else if(csvFile.equals(csvFileNames[7])) {
            	try {
					dbm.insertDataGender(row[1], row[2], Integer.parseInt(row[3]), Integer.parseInt(row[4]));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	System.out.println("Written it into the Database");
            }
        }

    } catch (FileNotFoundException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    } finally {
        if (br != null) {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
}
