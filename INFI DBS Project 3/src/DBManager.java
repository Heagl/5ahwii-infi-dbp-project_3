import java.awt.Frame;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.knowm.xchart.*;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;


public class DBManager {

	private static DBManager _instance;

	//Singleton
	private DBManager() throws ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
	}
	public static DBManager getInstance() throws ClassNotFoundException {
		if(_instance == null) {
			_instance = new DBManager();
		}
		return _instance;
	}

	//private Functions
	public Connection getConnection() throws SQLException {
		String dbname = "unternehmensdaten";
		String user = "schule";
		String pwd = "schule";
		String conStr = "jdbc:mysql://localhost:3306/unternehmensdaten?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Europe/Berlin";
		return (Connection) DriverManager.getConnection(conStr,user,pwd);
	}

	public void releaseConnection(Connection con) throws SQLException {
		con.close();
	}

	public void printDataGender() throws SQLException {
		Connection con = getConnection();
		java.sql.Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("select * from companies_gender");
		int i = 1;
		List<Double> resultsTemp = new ArrayList<Double>();
		List<Integer> resultsDate = new ArrayList<Integer>();
		try {
			con = getConnection();
			while(rs.next())
			{
				
			}

			XYChart chart = QuickChart.getChart("Unternehmensneugründungen nach Geschlecht", "X", "Y", "y(x)", resultsDate, resultsTemp);
			//System.out.println(chart);
			new SwingWrapper(chart).displayChart();
		}
		finally {
			if(rs != null) {
				rs.close();
			}

			i = 0;

			if(con != null) {
				this.releaseConnection(con);
			}
		}
	}

	
	public void printDataWomenPercentage() throws SQLException {
		Connection con = getConnection();
		java.sql.Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("select * from companies_women_percentage");
		int i = 1;
		List<Double> resultsTemp = new ArrayList<Double>();
		List<Integer> resultsDate = new ArrayList<Integer>();
		try {
			con = getConnection();
			while(rs.next())
			{
				
			}

			XYChart chart = QuickChart.getChart("Unternehmensneugründungen nach Frauenanteil in %", "X", "Y", "y(x)", resultsDate, resultsTemp);
			//System.out.println(chart);
			new SwingWrapper(chart).displayChart();
		}
		finally {
			if(rs != null) {
				rs.close();
			}

			i = 0;

			if(con != null) {
				this.releaseConnection(con);
			}
		}
	}

	
	public void printDataSectors() throws SQLException {
		Connection con = getConnection();
		java.sql.Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("select * from companies_sectors");
		int i = 1;
		List<Double> resultsTemp = new ArrayList<Double>();
		List<Integer> resultsDate = new ArrayList<Integer>();
		try {
			con = getConnection();
			while(rs.next())
			{
				
			}

			XYChart chart = QuickChart.getChart("Unternehmensneugründungen nach Branchen", "X", "Y", "y(x)", resultsDate, resultsTemp);
			//System.out.println(chart);
			new SwingWrapper(chart).displayChart();
		}
		finally {
			if(rs != null) {
				rs.close();
			}

			i = 0;

			if(con != null) {
				this.releaseConnection(con);
			}
		}
	}

	
	public void printDataState() throws SQLException {
		Connection con = getConnection();
		java.sql.Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("select * from companies_state");
		int i = 1;
		List<Double> resultsTemp = new ArrayList<Double>();
		List<Integer> resultsDate = new ArrayList<Integer>();
		try {
			con = getConnection();
			while(rs.next())
			{
				
			}

			XYChart chart = QuickChart.getChart("Unternehmensneugründungen nach Bundesland", "X", "Y", "y(x)", resultsDate, resultsTemp);
			//System.out.println(chart);
			new SwingWrapper(chart).displayChart();
		}
		finally {
			if(rs != null) {
				rs.close();
			}

			i = 0;

			if(con != null) {
				this.releaseConnection(con);
			}
		}
	}

	
	public void printDataLegalForm() throws SQLException {
		Connection con = getConnection();
		java.sql.Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("select * from companies_legal_form");
		int i = 1;
		List<Double> resultsTemp = new ArrayList<Double>();
		List<Integer> resultsDate = new ArrayList<Integer>();
		try {
			con = getConnection();
			while(rs.next())
			{
				
			}

			XYChart chart = QuickChart.getChart("Unternehmensneugründungen nach Rechtsformen", "X", "Y", "y(x)", resultsDate, resultsTemp);
			//System.out.println(chart);
			new SwingWrapper(chart).displayChart();
		}
		finally {
			if(rs != null) {
				rs.close();
			}

			i = 0;

			if(con != null) {
				this.releaseConnection(con);
			}
		}
	}

	
	public void printDataAgeAverage() throws SQLException {
		Connection con = getConnection();
		java.sql.Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("select * from companies_age_average");
		int i = 1;
		List<Double> resultsTemp = new ArrayList<Double>();
		List<Integer> resultsDate = new ArrayList<Integer>();
		try {
			con = getConnection();
			while(rs.next())
			{
				
			}

			XYChart chart = QuickChart.getChart("Unternehmensneugründungen nach Durchschnittsalter & Bundesland", "X", "Y", "y(x)", resultsDate, resultsTemp);
			//System.out.println(chart);
			new SwingWrapper(chart).displayChart();
		}
		finally {
			if(rs != null) {
				rs.close();
			}

			i = 0;

			if(con != null) {
				this.releaseConnection(con);
			}
		}
	}

	
	public void printDataAge() throws SQLException {
		Connection con = getConnection();
		java.sql.Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("select * from companies_age");
		int i = 1;
		List<Double> resultsTemp = new ArrayList<Double>();
		List<Integer> resultsDate = new ArrayList<Integer>();
		try {
			con = getConnection();
			while(rs.next())
			{
				
			}

			XYChart chart = QuickChart.getChart("Unternehmensneugründungen nach Altersgruppen", "X", "Y", "y(x)", resultsDate, resultsTemp);
			//System.out.println(chart);
			new SwingWrapper(chart).displayChart();
		}
		finally {
			if(rs != null) {
				rs.close();
			}

			i = 0;

			if(con != null) {
				this.releaseConnection(con);
			}
		}
	}
	
	public void insertDataAge(String agegroup, int year, int amount) throws SQLException {
		Connection con = null; 
		PreparedStatement stmt = null;
		String query = "Insert into companies_age (Agegroup, Year, Amount) values (?,?,?)";
		con = getConnection();
		stmt = con.clientPrepareStatement(query);
		stmt.setString(1, agegroup);
		stmt.setInt(2, year);
		stmt.setInt(3, amount);
		stmt.executeQuery();
		
	}
	
	public void insertDataLegalForm(String legalform, int year, int amount) throws SQLException {
		Connection con = null; 
		PreparedStatement stmt = null;
		String query = "Insert into companies_legal_form (Type, Year, Amount) values (?,?,?)";
		con = getConnection();
		stmt = con.clientPrepareStatement(query);
		stmt.setString(1, legalform);
		stmt.setInt(2, year);
		stmt.setInt(3, amount);
		stmt.executeQuery();
		
	}
	
	public void insertDataAgeAverage(String state, String cat, int year, int amount) throws SQLException {
		Connection con = null; 
		PreparedStatement stmt = null;
		String query = "Insert into companies_age_average (state, categorie, Year, Amount) values (?,?,?,?)";
		con = getConnection();
		stmt = con.clientPrepareStatement(query);
		stmt.setString(1, state);
		stmt.setString(2, cat);
		stmt.setInt(3, year);
		stmt.setInt(4, amount);
		stmt.executeQuery();
	}
	
	public void insertDataState(String state, int year, int amount) throws SQLException {
		Connection con = null; 
		PreparedStatement stmt = null;
		String query = "Insert into companies_state (state, Year, Amount) values (?,?,?)";
		con = getConnection();
		stmt = con.clientPrepareStatement(query);
		stmt.setString(1, state);
		stmt.setInt(2, year);
		stmt.setInt(3, amount);
		stmt.executeQuery();
	}
	
	public void insertDataSectors(String sector, int year, int amount) throws SQLException {
		Connection con = null; 
		PreparedStatement stmt = null;
		String query = "Insert into companies_state (sector, Year, Amount) values (?,?,?)";		
		con = getConnection();
		stmt = con.clientPrepareStatement(query);
		stmt.setString(1, sector);
		stmt.setInt(2, year);
		stmt.setInt(3, amount);
		stmt.executeQuery();
	}
	
	public void insertDataWomenPercentage(String state, String cat, int year, int amount) throws SQLException {
		String query = "Insert into companies_women_percentage (State, Categorie, Year, Amount) values (?,?,?,?)";
		Connection con = null; 
		PreparedStatement stmt = null;
		con = getConnection();
		stmt = con.clientPrepareStatement(query);
		stmt.setString(1, state);
		stmt.setString(2, cat);
		stmt.setInt(3, year);
		stmt.setInt(4, amount);
		stmt.executeQuery();
	}
	
	public void insertDataGender(String state, String cat, int year, int amount) throws SQLException {
		String query = "Insert into companies_gender (State, Categorie, Year, Amount) values (?,?,?,?)";
		Connection con = null; 
		PreparedStatement stmt = null;
		con = getConnection();
		stmt = con.clientPrepareStatement(query);
		stmt.setString(1, state);
		stmt.setString(2, cat);
		stmt.setInt(3, year);
		stmt.setInt(4, amount);
		stmt.executeQuery();
	}
	
}
